/**
 * The Game of patience main class
 *
 * @author Chris Loftus and Lynda Thomas
 * @version 2.0
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import javafx.application.Application;
import javafx.stage.Stage;
import uk.ac.aber.dcs.cs12320.cards.gui.javafx.CardTable;

public class Game extends Application {

    private CardTable cardTable;
    private Scanner scan;
    private Deck unpackedCards;
    private Deck displayedCards;
    private String filename;
    private ArrayList<String> cardStrings;

    public Game() {
        unpackedCards = new Deck();
        displayedCards = new Deck();
        cardStrings = new ArrayList<>();
    }

    @Override
    public void start(Stage stage) {
        cardTable = new CardTable(stage);

        // The interaction with this game is from a command line
        // menu. We need to create a separate non-GUI thread
        // to run this in. DO NOT REMOVE THIS.
        Runnable commandLineTask = () -> {
            // REPLACE THE FOLLOWING EXAMPLE WITH YOUR CODE
            initialise();
            playGame();
        };
        Thread commandLineThread = new Thread(commandLineTask);
        // This is how we start the thread.
        // This causes the run method to execute.
        commandLineThread.start();
    }

    private void initialise() {
        filename = "cards.txt";
        System.out.println("Using file " + filename);
        try {
            unpackedCards.load(filename);
        } catch (FileNotFoundException e) {
            System.err.println("The file: " + filename + " does not exist. Assuming first use and an empty file." +
                    " If this is not the first use then have you accidentally deleted the file?");
        } catch (IOException e) {
            System.err.println("An unexpected error occurred when trying to open the file " + filename);
            System.err.println(e.getMessage());
        }
    }
    private void displayTheTable(){
        cardTable.cardDisplay(cardStrings);
    }

    private void cardsOnTheTable(){
        cardStrings.clear();
        for (Card c : displayedCards.getCards()){
            cardStrings.add(c.toString() + ".gif");
        }
    }
    private void playGame() {
        String response;
        do {
            printMenu();
            System.out.println("What would you like to do:");
            scan = new Scanner(System.in);
            response = scan.nextLine().toUpperCase();
            switch (response) {
                case "1":
                    unpackedCards.shuffleTheCards();
                    displayTheTable();
                    break;
                case "2":
                    displayAllUnpacked();
                    break;
                case "3":
                    dealCard();
                    break;
                case "4":
                    displayedCards.lastOntoChosen(2);
                    cardsOnTheTable();
                    displayTheTable();
                    break;
                case "5":
                    displayedCards.lastOntoChosen(4);
                    cardsOnTheTable();
                    displayTheTable();
                    break;
                case "6":
                    displayedCards.amalgamatingInTheMiddle();
                    cardsOnTheTable();
                    displayTheTable();
                    break;
                case "7":
                    displayAll();
                    break;
                case "8":
                    action();
                    break;
                case "9":
                    multipleAction();
                    break;
                case "Q":
                    break;
                default:
                    System.out.println("Try again");
            }
        } while (!(response.equals("Q")));
    }

    private void displayAllUnpacked() {
        System.out.println(unpackedCards);
    }

    private void displayAll() {
        System.out.println(displayedCards);
    }

    private void dealCard() {
        Card pickedCard = unpackedCards.getCards().get(0);
        displayedCards.getCards().add(pickedCard);
        unpackedCards.getCards().remove(pickedCard);
        cardsOnTheTable();
        displayTheTable();

    }

    private void action() {
        int size = displayedCards.getCards().size();
        if (size > 1) {
            Card c = displayedCards.getCards().get(size - 1);
            if (c.suitOrValue(displayedCards.getCards().get(size - 2))) {
                displayedCards.lastOntoChosen(2);
                cardsOnTheTable();
                displayTheTable();
            } else if (c.suitOrValue(displayedCards.getCards().get(size - 2))) {
                displayedCards.lastOntoChosen(4);
                cardsOnTheTable();
                displayTheTable();
            } else {
                dealCard();
            }
        } else {
            dealCard();
        }
    }

    private void multipleAction() {
        scan = new Scanner(System.in);
        boolean state = true;
        int howMany = 0;
        while (state) {
            try {
                System.out.println("How many moves do you want to be generated? ");
                howMany = scan.nextInt();
                state = false;
            } catch (InputMismatchException e) {
                System.err.println("Input number, try again ");
                scan = new Scanner(System.in);
            }
        }
        if (howMany > 0) {
            while (howMany > 0) {
                action();
                howMany = howMany - 1;
            }
        }
    }

    private void printMenu() {
        System.out.println("Choose what you want to do with a deck of cards");
        System.out.println("1  - Shuffle the cards.");
        System.out.println("2  - Show the pack (this is so you can check that it plays properly).");
        System.out.println("3  - Deal a card.");
        System.out.println("4  - Make a move, move last pile onto previous one.");
        System.out.println("5  - Make a move, move last pile back over two piles.");
        System.out.println("6  - Amalgamate piles in the middle (by giving their numbers).");
        System.out.println("7  - Show all the displayed cards in text form on the command line.");
        System.out.println("8  - Play for me once");
        System.out.println("9  - Play for me a number of times (repeats Play for me once a given number of times).");
        System.out.println("10 - Show the top ten results of all games so far, sorted with best score at the top.");
        System.out.println("11 - Quit.");
    }

    // //////////////////////////////////////////////////////////////
    public static void main(String args[]) {
        Application.launch(args);
        Game game = new Game();
//        game.initialise();
//        game.playGame();
    }
}
