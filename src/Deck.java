import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Deck {
    private ArrayList<Card> cards;
    Scanner in;

    public Deck() {
        cards = new ArrayList<>();
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void shuffleTheCards() {
        Collections.shuffle(cards);
    }

    public void load(String infileName) throws IOException {
        // what it does is to automatically close the file after the try / catch ends.
        // This means we don't have to worry about closing the file.
        try (FileReader fr = new FileReader(infileName);
             BufferedReader br = new BufferedReader(fr);
             Scanner infile = new Scanner(br)) {

            // Use the delimiter pattern so that we don't have to clear end of line
            // characters
            infile.useDelimiter("\r?\n|\r");

            int numCards = 52;
            for (int i = 0; i < numCards; i++) {
                Card c = null;
                c = new Card();
                c.load(infile);

                if (c != null) {
                    cards.add(c);
                }
            }
        }
    }

    public void lastOntoChosen(int chosenCardFromTheEnd) {

        int size = cards.size();
        Card last = cards.get(size - 1);
        if (last.suitOrValue(cards.get(size - chosenCardFromTheEnd))) {
            cards.set(size - chosenCardFromTheEnd, last);
            cards.remove(size -1);
        } else {
            System.err.println("Cards cannot be amalgamated");
        }
    }

    public void amalgamatingInTheMiddle() {
        in = new Scanner(System.in);
        int onTheTop = 0;
        int covered = 0;
        boolean state = true;
        while (state) {
            try {
                System.out.println("Which pile is moving? ");
                onTheTop = in.nextInt() - 1;
                System.out.println("Where is it moving to? ");
                covered = in.nextInt() - 1;
                state = false;
            } catch (InputMismatchException e) {
                System.err.println("Input number, try again ");
                in = new Scanner(System.in);
            }
        }
        int absolute = Math.abs(onTheTop - covered);
        Card last = cards.get(onTheTop);
        if (absolute == 1 || absolute == 3) {
            if (last.suitOrValue(cards.get(covered))) {
                cards.set(covered, last);
                cards.remove(onTheTop);
            } else {
                System.err.println("Cards cannot be amalgamated");
            }
        } else {
            System.err.println("You can amalgamate only two next to each other or two piles between ");
        }
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append("Deck of cards:");
        sb.append("\n");
        int makeEnter = 13;
        int currentIndex;
        for (Card c : cards) {
            currentIndex = cards.indexOf(c);
            if (currentIndex == makeEnter) {
                sb.append("\n");
                makeEnter = makeEnter + 13;
            }
            sb.append(c.toString());
            sb.append(" ");
        }
        sb.append("\n");
        return sb.toString();
    }
}
