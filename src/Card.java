import java.util.Scanner;

public class Card implements Comparable<Card> {
    private String value, suit;

    public Card() {
        this("unknown", "unknown");
    }

    public Card(String value, String suit) {
        this.value = value;
        this.suit = suit;
    }

    public String getSuit() {
        return suit;
    }

    public String getValue() {
        return value;
    }

    public void load(Scanner infile) {

        value = infile.next();
        suit = infile.next();
    }

    public boolean suitOrValue(Card c) {
        if (value.equals(c.getValue())) return true;

        return suit.equals(c.getSuit());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append(value);
        sb.append(suit);
        return sb.toString();
    }

    @Override
    public int compareTo(Card other) {
        return value.toUpperCase().compareTo(other.value.toUpperCase());
    }
}
